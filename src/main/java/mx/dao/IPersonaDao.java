package mx.dao;

import org.springframework.data.repository.CrudRepository;
import mx.domain.Persona;


public interface IPersonaDao extends CrudRepository<Persona, Long>{
    
    /*Definiendo esta interface y extendiendo de otra
    no necesitamos hacer los metodos del crud, porque ya 
    estan plasmados dentro de CrudRepository
    
    
    
    Dentro de <> lleva en primera opcion el tipo de dato a autilizar
    y despues el tipo de llave primaria*/
}
